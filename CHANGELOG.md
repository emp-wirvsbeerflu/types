# Changelog

## [0.9.0]

- added new shoppinglist types `HARDWARESTORE` and `OTHER` to `shoppinglist.ts` - [HH20-5](https://helping-hands.atlassian.net/browse/HH20-5)

## [0.8.0]

- add optional `items` field to `/shoppinglist/:id/deliver` this is used to mark partially resolved lists

## [0.7.6]

- add `comment` to `ShoppingListBase`
- add `dueDate` to `ShoppingListBase`, supposed to be a unix timestamp (can be generated with `Date.now()`)

## [0.7.5]

- add `budget` to `ShoppingListBase`

## [0.7.4]

- add `notificationToken` to type `User`
- add route `DELETE /user`
- add `openTasks` to `GET /user` return value

## [0.7.3]

- `/homie/lists` might not always be able to return `ShoppingListWithUser`, lists might not have a `takenBy` field

## [0.7.2]

- add Document to `ShoppingListWithUser`

## [0.7.1]

- add state `ON_DELIVERY` and `CLOSED`
- add option `done` to `ListItem`
- change type `SortedShoppingLists` to `ShoppingListWithUser`
- route `GET /homies/:page?`
  - change prop `paging` to `hasNext`
  - add prop `totalHomies`
- change route `GET /user/lists` to `GET /homie/lists`
- add route `GET /hero/lists`
- change route `POST /shoppinglist/:id` to `GET /shoppinglist/:id/edit`
- add route `GET /shoppinglist/:id` (fancy backend magic)
- add route `POST /shoppinglist/:id/publish`
- add route `POST /shoppinglist/:id/unpublish`
- add route `POST /shoppinglist/:id/untake`
- add route `POST /shoppinglist/:id/deliverd`
- add route `POST /shoppinglist/:id/close`
- add route `POST /shoppinglist/:id/update-items`

## [0.7.0]

- document responses that the backend currently gives but aren't defined anywhere
- move some inline definitions of types into named definitions to make usage easier
- new type `SuccessResponse`
- new type `UserWithHomies`
- new type `UserWithShoppingLists`
- new type `SortedShoppingLists`

## [0.7.0-beta4]

- added route `DELETE /shoppinglist/:id`
- added route `GET /user/lists` to get Users lists
- removed lists from `GET /user`

## [0.7.0-beta3]

- fix `POST /shoppinglist/take` params, because we can't send array as payload
- add prop `phone` to `User`

## [0.7.0-beta2]

- fix `takenBy` at route `GET /user`

## [0.7.0-beta1]

- rework `UserType` to `enum`

## [0.6.0-beta1]

- new type `ShoppingListApi` for API Definition
- add typedoc and deploy to gitlab pages

## [0.5.2]

- new type `ShoppingListWithUser`
- new type `UserWithOwnShoppingLists`

## [0.5.1]

- restore `UserUpdatePostData`

## [0.5.0]

- new type `ShoppingListUpdatePostData`
- construct the update Types with `Partial` and `Omit` to prevent copy&paste errors

## [0.4.0]

- copy&paste eslint/prettier setup
- rename `NEEDS_HELP` to `HOMIE`

## [0.3.1]

- too dumb to merge

## [0.3.0]

- remove count from shoppinglist
- remove all the various shopping list change types, will be replaced by an
  update thing

## [0.2.1]

- remove tar file from published package

## [0.2.0]

- add an index file

## [0.1.0]

- Initial commit
