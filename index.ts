import { Document, DocumentID, SuccessResponse } from './src/generic';
import { ShoppingList, ShoppingListBase, ShoppingListWithUser } from './src/shoppinglist';
import { User, UserWithHomies } from './src/user';
import { UserWithShoppingLists } from './src/userwithshoppinglists';

export * from './src/generic';
export * from './src/shoppinglist';
export * from './src/user';

/**
 * # API Definition
 *
 * defines the target-state of our api.
 *
 * ## Useful links
 * - staging: https://staging-shoppinglist-dot-emp-wirvsbeerflu.appspot.com/
 * - prod: https://shoppinglist-dot-emp-wirvsbeerflu.appspot.com/
 *
 * ## Usage
 * ```ts
 * const foo: ShoppingListApi['/shoppinglist/:id']['GET']['return'];
 * ```
 *
 * ## Authentication
 *
 * All Requests to this API allready have the authentiation key from Firebase
 */
export type ShoppingListApi = {
  // === Create or update User ===
  '/user': {
    /**
     * Creates a new user
     */
    POST: {
      params: User;
      return: Document<User>;
    };
    /**
     * Override some data for authenticated user
     */
    PUT: {
      params: Partial<User>;
      return: SuccessResponse;
    };
    /**
     * Get authenticated user
     */
    GET: {
      return: User & { openTasks: number };
    };
    /**
     * User Deletes his account
     * Backend should delete also lists from user
     */
    DELETE: {};
  };
  '/homie/lists': {
    /**
     * Get all lists created by the authenticated User with hero user data (Homie)
     */
    GET: {
      return: ShoppingListWithUser[] | Document<ShoppingList>[];
    };
  };
  '/hero/lists': {
    /**
     * Get all lists taken by the authenticated User with homie data (Hero)
     */
    GET: {
      return: ShoppingListWithUser[];
    };
  };
  // === Create or update shoppinglist ===
  '/shoppinglist': {
    /**
     * Create new ShoppingList
     */
    POST: {
      params: ShoppingListBase;
      return: Document<ShoppingList>;
    };
  };
  '/shoppinglist/:id': {
    /**
     * edit existing ShoppingList
     */
    PUT: {
      params: Partial<ShoppingListBase>;
      return: Document<ShoppingList>;
    };
    /**
     * Get ShoppingList by Id.
     * @fancy_backend_magic
     */
    GET: {
      return: ShoppingListWithUser;
    };
    /**
     * Delete ShoppingList by id
     */
    DELETE: {};
  };
  /**
   * Get ShoppingList by Id.
   * >**important!** - This is beeing used for editing an existing List.
   * Backend needs to set state to {@link ShoppingListState.IN_CREATION} to avoid conflicts!
   * @unexpected_backend_magic
   */
  '/shoppinglist/:id/edit': {
    GET: {
      return: ShoppingListBase;
    };
  };
  // === Change ShoppingList State ===
  // SET STATE = PUBLISHED
  // called by homie
  '/shoppinglist/:id/publish': {
    POST: {
      params: {};
      return: SuccessResponse;
    };
  };
  // SET STATE = IN_CREATION
  // called by homie
  '/shoppinglist/:id/unpublish': {
    POST: {
      params: {};
      return: SuccessResponse;
    };
  };
  // SET STATE = TAKEN
  // called by hero
  '/shoppinglist/take': {
    POST: {
      /** ShoppingList IDs. 1-n possible */
      params: {
        shoppinglists: DocumentID[];
      };
      return: SuccessResponse;
    };
  };
  // SET STATE = PUBLISHED
  // called by hero
  '/shoppinglist/:id/untake': {
    POST: {
      params: {};
      return: SuccessResponse;
    };
  };
  // SET STATE = ON_DELIVERY
  // called by hero
  '/shoppinglist/:id/deliver': {
    POST: {
      params: {
        price: number;
        /** optional array to mark the list as a partial deliver the booleans mark items that will be delivered (true=included) */
        items?: boolean[];
      };
      return: SuccessResponse;
    };
  };
  // SET STATE = DELIVERED
  // called by hero
  '/shoppinglist/:id/delivered': {
    POST: {
      params: {};
      return: SuccessResponse;
    };
  };
  // SET STATE = CLOSED
  // called by homie
  '/shoppinglist/:id/close': {
    POST: {
      params: {};
      return: SuccessResponse;
    };
  };
  // Update items
  '/shoppinglist/:id/update-items': {
    POST: {
      params: {
        items: boolean[];
      };
      return: SuccessResponse;
    };
  };
  // === Get Homies ===
  '/homies/:page?': {
    /**
     * Return Users that need Help
     * - sorted by distance, calculated by locations
     * - only count on published ShoppingLists
     * - at least one published ShoppingList
     * @param page optional for paging
     */
    GET: {
      return: UserWithHomies;
    };
  };
  '/homie/:id': {
    /**
     * @param id Homies UserID
     * - only show published ShoppingLists
     */
    GET: UserWithShoppingLists;
  };
};

export default ShoppingListApi;
