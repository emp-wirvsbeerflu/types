import { UserDisplayInformation } from './user';
import { Document } from './generic';
import { ShoppingListBase } from './shoppinglist';

export type UserWithShoppingLists = {
  return: UserDisplayInformation & {
    shoppinglists: Document<ShoppingListBase>[];
  };
};
