export type DocumentID = string;

export type Document<T> = T & {
  id: DocumentID;
  apiVersion: number;
};

export type SuccessResponse = {
  result: 'ok';
};
