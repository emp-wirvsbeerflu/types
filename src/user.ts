import { Document } from './generic';
// export type UserType = "HERO" | "HOMIE";
export enum UserType {
  HERO = 'HERO',
  HOMIE = 'HOMIE',
}

export const USER_API_VERSION = 1;

export type UserDisplayInformation = {
  displayName: string;
  photoURL: string;
};

export type Address = {
  city: string;
  street: string;
  houseNumber: string;
  postalCode: string;
};

export type User = UserDisplayInformation & {
  type: UserType;

  // Firebase Push Notification Token
  notificationToken?: string;

  phone?: string;

  address?: Address;

  location?: {
    lat: number;
    lng: number;
  };
  // only defined/needed if type === HERO
  radius?: number;
};

export type UserWithHomies = {
  /** Search radius, rounded in km */
  radius: number;
  /** Is there a next page? */
  hasNext: boolean;
  /** total count of matching homies */
  totalHomies: number;
  /** list of homies sorted by distance */
  homies: (Document<UserDisplayInformation> & {
    shoppinglistCount: number;
    /** rounded distance in km */
    distance: number;
    pharmacy?: boolean;
  })[];
};
