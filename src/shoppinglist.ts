import { DocumentID, Document } from './generic';
import { UserDisplayInformation, User, Address } from './user';

export const SHOPPINGLIST_API_VERSION = 1;

export enum ShoppingListState {
  IN_CREATION = 'IN_CREATION',
  PUBLISHED = 'PUBLISHED',
  TAKEN = 'TAKEN',
  ON_DELIVERY = 'ON_DELIVERY',
  DELIVERED = 'DELIVERED',
  CLOSED = 'CLOSED',
}

export enum ShoppingListType {
  SUPERMARKET = 'SUPERMARKET',
  PHARMACY = 'PHARMACY',
  MAIL = 'MAIL',
  DRUGSTORE = 'DRUGSTORE',
  HARDWARESTORE = 'HARDWARESTORE',
  OTHER = 'OTHER',
}

export type ListItem = {
  name: string;
  count: number;
  done: boolean;
};

export type ShoppingListBase = {
  type: ShoppingListType;
  items: ListItem[];
  comment?: string;
  dueDate?: number;
};

export type ShoppingList = ShoppingListBase & {
  user: DocumentID;
  state: ShoppingListState;
  takenBy?: DocumentID;
  price?: number;
};

export type ShoppingListWithUser = Document<ShoppingList> &
  UserDisplayInformation & {
    // only on TAKEN
    phone?: string;
    // only on IN_DELIVERY
    address?: Address;
  };
